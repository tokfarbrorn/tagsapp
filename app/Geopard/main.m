//
//  main.m
//  Geopard
//
//  Created by Mikael Sundberg on 2013-02-14.
//  Copyright (c) 2013 Mikael Sundberg. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "msGeopardAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([msGeopardAppDelegate class]));
    }
}
