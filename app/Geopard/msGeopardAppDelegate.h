//
//  msGeopardAppDelegate.h
//  Geopard
//
//  Created by Mikael Sundberg on 2013-02-14.
//  Copyright (c) 2013 Mikael Sundberg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@protocol GeopardLocationUpdateDelegate <NSObject>
@required
- (void)hasNewLocation:(CLLocation *) location;
- (void)needsReload;
@end

@interface msGeopardAppDelegate : UIResponder <UIApplicationDelegate, CLLocationManagerDelegate>
{
    CLLocation *currentLocation;
    float maxRange;
    int memberId;
}
@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) CLLocationManager *locationManager;
@property (nonatomic, assign) id <GeopardLocationUpdateDelegate> locationUpdateDelegate;

-(void)startStandardUpdates;
-(CLLocation *)getCurrentLocation;
-(void)SetNeedsReload;
-(float)GetMaxRange;
-(void)SetMaxRange: (float)range;
-(int)GetMemberId;
-(void)SetMemberId: (int)memberid;
@end
