//
//  msGeopardAppDelegate.m
//  Geopard
//
//  Created by Mikael Sundberg on 2013-02-14.
//  Copyright (c) 2013 Mikael Sundberg. All rights reserved.
//

#import "msGeopardAppDelegate.h"
#import <CoreLocation/CoreLocation.h>

@implementation msGeopardAppDelegate

@synthesize locationManager;
@synthesize locationUpdateDelegate;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    [self startStandardUpdates];
    maxRange = 250;
    memberId = 1;
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)startStandardUpdates
{
    // Create the location manager if this object does not
    // already have one.
    if (nil == locationManager)
        locationManager = [[CLLocationManager alloc] init];
    
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyKilometer;
    
    // Set a movement threshold for new events.
    locationManager.distanceFilter = 50;
    
    [locationManager startUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation* location = [locations lastObject];
    NSDate* eventDate = location.timestamp;
    NSTimeInterval howRecent = [eventDate timeIntervalSinceNow];
    if (abs(howRecent) < 15.0)
    {
        // If the event is recent, do something with it.
        NSLog(@"latitude %+.6f, longitude %+.6f\n",
              location.coordinate.latitude,
              location.coordinate.longitude);
        currentLocation = location;
        if (locationUpdateDelegate != nil && [locationUpdateDelegate respondsToSelector:@selector(hasNewLocation:)])
        {
            [[self locationUpdateDelegate]hasNewLocation:location];
        }
    }
}

-(CLLocation *)getCurrentLocation
{
    return currentLocation;
}

-(float)GetMaxRange
{
    return maxRange;
}

-(void)SetMaxRange: (float)range
{
    maxRange = range;
}

-(int)GetMemberId
{
    return memberId;
}

-(void)SetMemberId: (int)memberid
{
    memberId = memberid;
}

-(void)SetNeedsReload
{
    [self.locationUpdateDelegate needsReload];
}

@end
