//
//  msGeopardDetailsViewController.h
//  Geopard
//
//  Created by Mikael Sundberg on 2013-02-18.
//  Copyright (c) 2013 Mikael Sundberg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface msGeopardDetailsViewController : UIViewController
{
    NSString *tagId;
}

-(void)SetTagId:(NSString *)theTagId;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *DoneButton;
@property (weak, nonatomic) IBOutlet UIWebView *WebView;

@end
