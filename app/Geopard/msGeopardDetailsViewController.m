//
//  msGeopardDetailsViewController.m
//  Geopard
//
//  Created by Mikael Sundberg on 2013-02-18.
//  Copyright (c) 2013 Mikael Sundberg. All rights reserved.
//

#import "msGeopardDetailsViewController.h"

@interface msGeopardDetailsViewController ()

@end

@implementation msGeopardDetailsViewController

@synthesize WebView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (IBAction)DoneButtonClick:(id)sender
{
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
        //
    }];
}

-(void)SetTagId:(NSString *)theTagId
{
    NSString *url = [[NSString alloc]initWithFormat:@"http://www.gethereapp.com/sitefile/tagdetails.php?tagid=%@&memberid=%@&key=%@", theTagId, @"1", @"apakaka"];
    NSURLRequest *req = [[NSURLRequest alloc] initWithURL:[[NSURL alloc] initWithString:url]];
    [WebView loadRequest:req];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
