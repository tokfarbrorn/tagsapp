//
//  msGeopardFirstViewController.h
//  Geopard
//
//  Created by Mikael Sundberg on 2013-02-14.
//  Copyright (c) 2013 Mikael Sundberg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

#import "msGeopardAppDelegate.h"
#import "msGeopardDetailsViewController.h"

@interface msGeopardFirstViewController : UITableViewController <UITableViewDataSource, GeopardLocationUpdateDelegate>
{
    bool hasNewLocation;
    bool isVisible;
    CLLocation *currentLocation;
    UISwipeGestureRecognizer *oneFingerSwipeLeft;
}

@property (strong, nonatomic) IBOutlet UITableView *TableView;
@property (strong, nonatomic) NSMutableArray *tags;
@property (strong, nonatomic) msGeopardDetailsViewController *detailsController;

-(void)loadTagsAtLocation: (CLLocation*)location;

@end
