//
//  msGeopardFirstViewController.m
//  Geopard
//
//  Created by Mikael Sundberg on 2013-02-14.
//  Copyright (c) 2013 Mikael Sundberg. All rights reserved.
//

#import "msGeopardFirstViewController.h"
#import "msGeopardAppDelegate.h"
#import "msGeopardTableCell.h"
#import "msGeopardDetailsViewController.h"

#import <CoreLocation/CoreLocation.h>
#import <RestKit/RestKit.h>

@interface msGeopardFirstViewController ()

@end

@implementation msGeopardFirstViewController

@synthesize detailsController;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.

    msGeopardAppDelegate *appDelegate = (msGeopardAppDelegate*)[[UIApplication sharedApplication]delegate];
    appDelegate.locationUpdateDelegate = self;
    
    oneFingerSwipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeLeft:)];
    [oneFingerSwipeLeft setDirection:UISwipeGestureRecognizerDirectionLeft];
    [self.TableView addGestureRecognizer:oneFingerSwipeLeft];

}

-(void)needsReload
{
    hasNewLocation = true;
}

-(void)viewDidAppear:(BOOL)animated
{
    isVisible = true;
    if(hasNewLocation)
        [self loadTagsAtLocation:currentLocation];
}

- (void)hasNewLocation:(CLLocation *) location;
{
    currentLocation = location;
    hasNewLocation = true;
    if(isVisible)
        [self loadTagsAtLocation: currentLocation];
}

-(void)loadTagsAtLocation: (CLLocation *)location
{
    
    hasNewLocation = false;

    [[self tags]removeAllObjects];

    msGeopardAppDelegate *appDelegate = (msGeopardAppDelegate*)[[UIApplication sharedApplication]delegate];
    float range = [appDelegate GetMaxRange];
    
    NSMutableArray *unsortedArray = [[NSMutableArray alloc]init];
    
    [RKClient clientWithBaseURL:[NSURL URLWithString:@"http://www.gethereapp.com/sitefile/"]];
    [[RKClient sharedClient] post:@"/rest.php" usingBlock:^(RKRequest *request)
     {
         RKParams *params = [RKParams paramsWithDictionary:[NSDictionary dictionaryWithKeysAndObjects:@"cmd", @"query",
                                                            @"memberid", [NSString stringWithFormat:@"%i", [appDelegate GetMemberId]],
                                                            @"key", @"apakaka",
                                                            @"radius", [[NSString alloc] initWithFormat:@"%0.0f", range],
                                                            @"longitude", [NSString stringWithFormat:@"%+.6f", location.coordinate.longitude],
                                                            @"latitude", [NSString stringWithFormat:@"%+.6f", location.coordinate.latitude],
                                                            nil]];
         [request setParams:params];
         request.onDidLoadResponse = ^(RKResponse *response)
         {
             
             NSLog(@"RestkitReponse %@", [response bodyAsString]);
             NSError *jsonError = [[NSError alloc]init];
             NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:response.body options:NSJSONReadingAllowFragments error:&jsonError];
             NSString *result = [jsonObject objectForKey:@"result"];
             
             if([result isEqualToString:@"ok"])
             {
                 NSArray *tags = [jsonObject objectForKey:@"tags"];
                 NSLog(@"Size of tags %i", tags.count);
                 for(int i = 0; i  < tags.count; i++)
                 {
                     NSString *data = [tags objectAtIndex:i];
                     NSData *jsonPayload = [data dataUsingEncoding:NSUTF8StringEncoding];
                     NSDictionary *tag = [NSJSONSerialization JSONObjectWithData:jsonPayload options:NSJSONReadingAllowFragments error:&jsonError];
                     [unsortedArray insertObject:tag atIndex:i];
                 }
             }
             NSLog(@"Result %@", result);
             
             NSArray *sortedArray = [unsortedArray sortedArrayUsingComparator:^NSComparisonResult(NSDictionary *a, NSDictionary *b)
             {
                 
                 NSArray *loc1 = [a objectForKey:@"loc"];
                 NSArray *loc2 = [b objectForKey:@"loc"];
                 
                 double long1 = [[loc1 objectAtIndex:0] floatValue];
                 double lat1 = [[loc1 objectAtIndex:1] floatValue];
                 double long2 = [[loc2 objectAtIndex:0] floatValue];
                 double lat2 = [[loc2 objectAtIndex:1] floatValue];
                 
                 CLLocation *locA = [[CLLocation alloc] initWithLatitude:lat1 longitude:long1];
                 CLLocation *locB = [[CLLocation alloc] initWithLatitude:lat2 longitude:long2];
                 
                 CLLocationDistance distanceA = [locA distanceFromLocation:currentLocation];
                 CLLocationDistance distanceB = [locB distanceFromLocation:currentLocation];
                 
                 if(distanceA < distanceB)
                     return NSOrderedAscending;
                 
                 if(distanceA > distanceB)
                     return NSOrderedDescending;
                 
                 return NSOrderedSame;
             }];
             self.tags = [[NSMutableArray alloc] initWithArray:sortedArray];
             
             [[self tableView] reloadData];
         };
         
     }];
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"";
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    int c =  [[self tags]count];
    NSLog(@"Having %i tags stored",[self.tags count]);
    return c;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"tagCell";
    
    msGeopardTableCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[msGeopardTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell...
    NSDictionary *tag = [[self tags] objectAtIndex:indexPath.row];
    
    NSError *jsonError = [[NSError alloc]init];

    NSArray *loc = [tag objectForKey:@"loc"];

    NSString *data = [tag objectForKey:@"data"];
    NSData *jsonPayload = [data dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *tagData = [NSJSONSerialization JSONObjectWithData:jsonPayload options:NSJSONReadingAllowFragments error:&jsonError];
    
    NSString *type = [tag objectForKey:@"type"];
    
    if([type isEqualToString:@"poi"])
    {
        cell.TextLabel.text = [tagData objectForKey:@"name"];
    }
    else if([type isEqualToString:@"note"])
    {
        cell.TextLabel.text = [tagData objectForKey:@"note"];
    }
    else if([type isEqualToString:@"url"])
    {
        cell.TextLabel.text = [tagData objectForKey:@"URL"];
    }
    else
    {
        cell.TextLabel.text = [tagData objectForKey:@"description"];
    }
    
    if([type isEqualToString:@"poi"])
        cell.TypeImage.image = [UIImage imageNamed:@"poi.png"];
    if([type isEqualToString:@"url"])
        cell.TypeImage.image = [UIImage imageNamed:@"url.png"];
    if([type isEqualToString:@"note"])
        cell.TypeImage.image = [UIImage imageNamed:@"note.png"];
    if([type isEqualToString:@"document"])
        cell.TypeImage.image = [UIImage imageNamed:@"document.png"];
    if([type isEqualToString:@"image"])
        cell.TypeImage.image = [UIImage imageNamed:@"image.png"];

    cell.accessoryType = UITableViewCellAccessoryNone;
    
    double long1 = [[loc objectAtIndex:0] floatValue];
    double lat1 = [[loc objectAtIndex:1] floatValue];
    
    CLLocation *locA = [[CLLocation alloc] initWithLatitude:lat1 longitude:long1];
    
    CLLocationDistance distance = [locA distanceFromLocation:currentLocation];
   
    cell.DistanceLabel.text = [[NSString alloc]initWithFormat:@"%0.0f m", distance];
    
    NSString *tagStr = [tagData objectForKey:@"tags"];
    cell.TagsLabel.text = tagStr;
    
    cell.NameLabel.text = [[NSString alloc]initWithFormat:@"User number %i", [[tag objectForKey:@"ownerid"] intValue]];
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    msGeopardAppDelegate *appDelegate = (msGeopardAppDelegate*)[[UIApplication sharedApplication]delegate];
    // Return YES if you want the specified item to be editable.
    NSDictionary *tag = [[self tags] objectAtIndex:indexPath.row];
    NSNumber *memberId = [tag objectForKey:@"ownerid"];
    if([memberId intValue] == [appDelegate GetMemberId])
        return YES;
    return NO;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        msGeopardAppDelegate *appDelegate = (msGeopardAppDelegate*)[[UIApplication sharedApplication]delegate];
        NSDictionary *tag = [[self tags] objectAtIndex:indexPath.row];
        [RKClient clientWithBaseURL:[NSURL URLWithString:@"http://www.gethereapp.com/sitefile/"]];
        [[RKClient sharedClient] post:@"/rest.php" usingBlock:^(RKRequest *request)
         {
             RKParams *params = [RKParams paramsWithDictionary:[NSDictionary dictionaryWithKeysAndObjects:@"cmd", @"delete",
                                                                @"memberid", [NSString stringWithFormat:@"%i", [appDelegate GetMemberId]],
                                                                @"key", @"apakaka",
                                                                @"tagid", [tag objectForKey:@"id"],
                                                                nil]];
             [request setParams:params];
             request.onDidLoadResponse = ^(RKResponse *response)
             {
                 NSLog(@"RestkitReponse %@", [response bodyAsString]);
                 NSError *jsonError = [[NSError alloc]init];
                 NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:response.body options:NSJSONReadingAllowFragments error:&jsonError];
                 NSString *result = [jsonObject objectForKey:@"result"];
                 if([result isEqualToString:@"ok"])
                 {
                     [self.tags removeObjectAtIndex:indexPath.row];
                 }
                 NSLog(@"Result %@", result);
                 [[self tableView] reloadData];
             };
             
         }];
    }
}

-(void)swipeLeft:(UISwipeGestureRecognizer *)recognizer
{
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSDictionary *tag = [[self tags] objectAtIndex:indexPath.row];
       
    if(detailsController == NULL)
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPhone" bundle:nil];
        detailsController = [storyboard instantiateViewControllerWithIdentifier:@"tagDetailsView"];
    }
    
    [self.navigationController pushViewController:detailsController animated:YES];
    [detailsController SetTagId:[tag objectForKey:@"id"]];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
