//
//  msGeopardImageTag.h
//  Geopard
//
//  Created by Mikael Sundberg on 2013-02-15.
//  Copyright (c) 2013 Mikael Sundberg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "msGeopardTagView.h"

@interface msGeopardImageTag : msGeopardTagView <UINavigationControllerDelegate, UIImagePickerControllerDelegate, UITextViewDelegate>
{
    UIImagePickerController *picker;
}

@property (weak, nonatomic) IBOutlet UITextView *Description;
@property (weak, nonatomic) IBOutlet UIImageView *Image;

@end
