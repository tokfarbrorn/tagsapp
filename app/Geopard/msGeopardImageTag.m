//
//  msGeopardImageTag.m
//  Geopard
//
//  Created by Mikael Sundberg on 2013-02-15.
//  Copyright (c) 2013 Mikael Sundberg. All rights reserved.
//

#import <MobileCoreServices/MobileCoreServices.h>
#import <RestKit/RestKit.h>
#import "msGeopardImageTag.h"
#import "msGeopardAppDelegate.h"

@implementation msGeopardImageTag

@synthesize Description;
@synthesize Image;

- (id)initWithFrame:(CGRect)frame
{
    isNew = true;
    self = [super initWithFrame:frame];
    if (self)
    {
        [Description setDelegate:self];
    }
    return self;
}

-(void)textViewDidBeginEditing:(UITextView *)textView
{
    if(isNew)
    {
        Description.text = @"";
        isNew = false;
    }
}

-(void)openCamera: (UIImagePickerControllerSourceType)type
{
    
    picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
       
    if ([UIImagePickerController isSourceTypeAvailable:type])
    {
        picker.sourceType = type;
        picker.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeImage, nil];;
        [[self GetViewController] presentViewController:picker animated:YES completion:^{
            
        }];
    }
    
}

- (IBAction)CameraButtonClick:(id)sender
{
    [self openCamera:UIImagePickerControllerSourceTypeCamera];
}

- (IBAction)RollButtonClicked:(id)sender
{
    [self openCamera:UIImagePickerControllerSourceTypePhotoLibrary];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *) Picker
{
    [[self GetViewController]dismissViewControllerAnimated:YES completion:^{
        
    }];
}

- (void)imagePickerController:(UIImagePickerController *) Picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    Image.image = [info objectForKey:UIImagePickerControllerOriginalImage];
    [[self GetViewController] dismissViewControllerAnimated:YES completion:^{ }];
}

-(NSDictionary *)GetAttributes
{
    NSDictionary *dict = [[NSDictionary alloc]initWithObjectsAndKeys:Description.text, @"description", nil];
    return  dict;
}

-(NSString *)GetType
{
    return(@"image");
}

-(void)ResignResponder
{
    [Description resignFirstResponder];
}

-(void)TagSaved: (NSString *)withTagId
{
    [super TagSaved:withTagId];
    
    CGSize size = Image.image.size;
    
    int newWidth = size.width;
    int newHeight = size.height;
    
    if(newWidth > 600 || newHeight > 600)
    {
        double ratioX = 600.0 / size.width;
        double ratioY = 600.0 / size.height;
        if(ratioX < ratioY)
        {
            newWidth *= ratioX;
            newHeight *= ratioY;
        }
        else
        {
            newWidth *= ratioY;
            newHeight *= ratioY;
        }
            
    }
    
    CGSize newSize = CGSizeMake(newWidth, newHeight);
    
    UIGraphicsBeginImageContext(newSize);
    [Image.image drawInRect:CGRectMake(0,0, newSize.width, newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    msGeopardAppDelegate *appDelegate = (msGeopardAppDelegate*)[[UIApplication sharedApplication]delegate];
    [RKClient clientWithBaseURL:[NSURL URLWithString:@"http://www.gethereapp.com/sitefile/"]];
    [[RKClient sharedClient] post:@"/rest.php" usingBlock:^(RKRequest *request)
     {
         RKParams *params = [RKParams paramsWithDictionary:[NSDictionary dictionaryWithKeysAndObjects:@"cmd", @"uploadImage",
                                                            @"memberid", [NSString stringWithFormat:@"%i", [appDelegate GetMemberId]],
                                                            @"key", @"apakaka",
                                                            @"tagid", withTagId,
                                                            nil]];
         // Attach an Image from the App Bundle
         NSData* imageData = UIImagePNGRepresentation(newImage);
         [params setData:imageData MIMEType:@"image/png" forParam:@"image"];
         
         [request setParams:params];
         request.onDidLoadResponse = ^(RKResponse *response)
         {
             NSLog(@"RestkitReponse %@", [response bodyAsString]);
             NSError *jsonError = [[NSError alloc]init];
             NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:response.body options:NSJSONReadingAllowFragments error:&jsonError];
             NSString *result = [jsonObject objectForKey:@"result"];
             if(![result isEqualToString:@"ok"])
             {
                 UIAlertView *view = [[UIAlertView alloc]initWithTitle:@"Failed" message:@"Failed to upload the image... )=" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                 view.tag = 1;
                 view.delegate = self;
                 [view show];
             }
         };
     }];
    Description.text = @"Describe the image";
}

@end
