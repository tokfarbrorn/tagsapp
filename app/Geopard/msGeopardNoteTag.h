//
//  msGeopardNoteTag.h
//  Geopard
//
//  Created by Mikael Sundberg on 2013-02-15.
//  Copyright (c) 2013 Mikael Sundberg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "msGeopardTag.h"
#import "msGeopardTagView.h"

@interface msGeopardNoteTag : msGeopardTagView <UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UITextView *Note;

@end
