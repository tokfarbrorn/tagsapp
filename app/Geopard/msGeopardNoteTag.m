//
//  msGeopardNoteTag.m
//  Geopard
//
//  Created by Mikael Sundberg on 2013-02-15.
//  Copyright (c) 2013 Mikael Sundberg. All rights reserved.
//

#import "msGeopardNoteTag.h"

@implementation msGeopardNoteTag

@synthesize Note;

- (id)initWithFrame:(CGRect)frame
{
    isNew = true;
    self = [super initWithFrame:frame];
    if (self)
    {
        [Note setDelegate:self];
    }
    return self;
}

-(void)textViewDidBeginEditing:(UITextView *)textView
{
    if(isNew)
    {
        Note.text = @"";
        isNew = false;
    }
}

-(NSDictionary *)GetAttributes
{
    NSDictionary *dict = [[NSDictionary alloc]initWithObjectsAndKeys:Note.text,@"note", nil];
    return  dict;
}

-(NSString *)GetType
{
    return(@"note");
}

-(void)ResignResponder
{
    [Note resignFirstResponder];
}

@end
