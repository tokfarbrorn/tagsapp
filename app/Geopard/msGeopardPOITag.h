//
//  msGeopardPOITag.h
//  Geopard
//
//  Created by Mikael Sundberg on 2013-02-15.
//  Copyright (c) 2013 Mikael Sundberg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "msGeopardTag.h"
#import "msGeopardTagView.h"

@interface msGeopardPOITag : msGeopardTagView <UITextFieldDelegate>
{
    bool nameCleared;
    bool tagsCleared;
}
@property (weak, nonatomic) IBOutlet UITextField *Name;
@property (weak, nonatomic) IBOutlet UITextField *Tags;

@end
