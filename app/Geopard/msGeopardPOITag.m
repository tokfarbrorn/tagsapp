//
//  msGeopardPOITag.m
//  Geopard
//
//  Created by Mikael Sundberg on 2013-02-15.
//  Copyright (c) 2013 Mikael Sundberg. All rights reserved.
//

#import "msGeopardPOITag.h"

@implementation msGeopardPOITag

@synthesize Name;
@synthesize Tags;

- (id)initWithFrame:(CGRect)frame
{
    isNew = true;
    self = [super initWithFrame:frame];
    if (self)
    {
        [Name setDelegate:self];
        [Tags setDelegate:self];
    }
    return self;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if(!nameCleared && textField == Name)
    {
        Name.text = @"";
        nameCleared = true;
    }
    if(!tagsCleared && textField == Tags)
    {
        Tags.text = @"";
        tagsCleared = true;
    }
}

-(NSDictionary *)GetAttributes
{
    NSDictionary *dict = [[NSDictionary alloc]initWithObjectsAndKeys:Name.text, @"name",
                          Tags.text, @"tags",
                          nil];
    return  dict;
}

-(NSString *)GetType
{
    return(@"poi");
}

-(void)ResignResponder
{
    [Name resignFirstResponder];
    [Tags resignFirstResponder];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(void)TagSaved:(NSString *)withTagId
{
    [super TagSaved:withTagId];
    Name.text = @"Describe this location";
    Tags.text = @"#tags";
    nameCleared = false;
    tagsCleared = false;
}

@end
