//
//  msGeopardSecondViewController.h
//  Geopard
//
//  Created by Mikael Sundberg on 2013-02-14.
//  Copyright (c) 2013 Mikael Sundberg. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "msGeopardAppDelegate.h"

@interface msGeopardSecondViewController : UIViewController <UITextViewDelegate, UITextFieldDelegate, UIAlertViewDelegate,  UIScrollViewDelegate>
{
    bool _pageControlUsed;
    int _page;
    bool _loadedViews;
    int currentViewIndex;
}

@property (weak, nonatomic) IBOutlet UIView *TagsView;
@property (weak, nonatomic) IBOutlet UIButton *DoneButton;
@property (strong, nonatomic) NSMutableArray *tagViews;
@property (weak, nonatomic) IBOutlet UISwitch *PublicSwitch;

@end
