//
//  msGeopardSecondViewController.m
//  Geopard
//
//  Created by Mikael Sundberg on 2013-02-14.
//  Copyright (c) 2013 Mikael Sundberg. All rights reserved.
//

#import <RestKit/RestKit.h>

#import "msGeopardSecondViewController.h"
#import "msGeopardTag.h"
#import "msGeopardTagView.h"

@interface msGeopardSecondViewController ()

@end

@implementation msGeopardSecondViewController

@synthesize tagViews;
@synthesize TagsView;

- (void)viewDidLoad
{
    [super viewDidLoad];

	// Do any additional setup after loading the view, typically from a nib.
    tagViews = [[NSMutableArray alloc]init];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    
}

-(void)dismissKeyboard
{
    msGeopardTagView *view = [self.tagViews objectAtIndex:currentViewIndex];
    [view ResignResponder];
}

-(void)viewDidLayoutSubviews
{
    
}

-(void)showTab: (int) tabNum
{
    for(int i = 0; i < tagViews.count; i++)
    {
        msGeopardTagView *view = (msGeopardTagView*)[tagViews objectAtIndex:i];
        view.hidden = (i != tabNum);
    }
    currentViewIndex = tabNum;
}

- (IBAction)ImageTabClick:(id)sender
{
    [self showTab:0];
}

- (IBAction)NoteTabClicked:(id)sender
{
    [self showTab:1];
}

- (IBAction)PoiTabClicked:(id)sender
{
    [self showTab:2];
}

- (IBAction)UrlTabClicked:(id)sender
{
    [self showTab:3];
}

-(void)viewDidAppear:(BOOL)animated
{
    //[[self TextView]becomeFirstResponder];
    [super viewDidAppear:animated];
    
    if(!_loadedViews)
    {
        NSArray *nibContents = [[NSBundle mainBundle] loadNibNamed:@"TagViews" owner:self options:nil];
                
        for(int i = 0; i < nibContents.count; i++)
        {
            CGRect rect = CGRectMake(0, 38, 320, 148);
            msGeopardTagView *myView = [[nibContents objectAtIndex:i] initWithFrame:rect];
            myView.frame = rect;
            myView.backgroundColor = [UIColor clearColor];
            myView.hidden = i != 0;
            [myView SetController:[self parentViewController]];
            [tagViews addObject:myView];
            [TagsView addSubview:myView];
        }
        
        _loadedViews = true;
    }
    
    [self registerForKeyboardNotifications];
        
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)registerForKeyboardNotifications
{
    
    /*
     [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidShow:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification object:nil];

    
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center addObserver:self selector:@selector(didHide) name:UIKeyboardWillHideNotification object:nil];
    */
}
    
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 1)
    {
        UITabBarController *p = ((UITabBarController *)[self parentViewController]);
        msGeopardAppDelegate *appDelegate = (msGeopardAppDelegate*)[[UIApplication sharedApplication]delegate];
        [appDelegate SetNeedsReload];
        [p setSelectedIndex:0];
    }
}

- (IBAction)CancelButtonClick:(id)sender
{
    UITabBarController *p = ((UITabBarController *)[self parentViewController]);
    [p setSelectedIndex:0];
}

- (IBAction)DoneButtonClick:(id)sender
{

    msGeopardTagView *view = [self.tagViews objectAtIndex:currentViewIndex];
    
    NSString *tagType = [view GetType];
    NSDictionary *tagDict = [view GetAttributes];
    NSString *tagAccessType = self.PublicSwitch.on ? @"public": @"private";
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:tagDict
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];    
    NSString *tagData = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    msGeopardAppDelegate *appDelegate = (msGeopardAppDelegate*)[[UIApplication sharedApplication]delegate];
    CLLocation *location = [appDelegate getCurrentLocation];

    [RKClient clientWithBaseURL:[NSURL URLWithString:@"http://www.gethereapp.com/sitefile/"]];
    [[RKClient sharedClient] post:@"/rest.php" usingBlock:^(RKRequest *request)
     {
         RKParams *params = [RKParams paramsWithDictionary:[NSDictionary dictionaryWithKeysAndObjects:@"cmd", @"add",
                                                            @"memberid", [NSString stringWithFormat:@"%i", [appDelegate GetMemberId]],
                                                            @"key", @"apakaka",
                                                            @"type", tagType,
                                                            @"data", tagData,
                                                            @"access", tagAccessType,
                                                            @"longitude", [NSString stringWithFormat:@"%+.6f", location.coordinate.longitude],
                                                            @"latitude", [NSString stringWithFormat:@"%+.6f", location.coordinate.latitude],
                                                            nil]];
         [request setParams:params];
         request.onDidLoadResponse = ^(RKResponse *response)
         {
             NSLog(@"RestkitReponse %@", [response bodyAsString]);
             NSError *jsonError = [[NSError alloc]init];
             NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:response.body options:NSJSONReadingAllowFragments error:&jsonError];
             NSString *result = [jsonObject objectForKey:@"result"];
             if([result isEqualToString:@"ok"])
             {
                 [view TagSaved:[jsonObject valueForKey:@"id"]];
                 UIAlertView *view = [[UIAlertView alloc]initWithTitle:@"Tag saved" message:@"Cheers, you just tagged it" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                 view.tag = 1;
                 view.delegate = self;
                 [view show];
             }
         };
         
     }];
    
}

@end
