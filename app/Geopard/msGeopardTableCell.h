//
//  msGeopardTableCell.h
//  Geopard
//
//  Created by Mikael Sundberg on 2013-02-15.
//  Copyright (c) 2013 Mikael Sundberg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface msGeopardTableCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UILabel *TextLabel;
@property (weak, nonatomic) IBOutlet UIImageView *TypeImage;
@property (weak, nonatomic) IBOutlet UILabel *NameLabel;
@property (weak, nonatomic) IBOutlet UILabel *DistanceLabel;
@property (weak, nonatomic) IBOutlet UILabel *TagsLabel;

@end
