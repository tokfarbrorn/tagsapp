//
//  msGeopardTag.h
//  Geopard
//
//  Created by Mikael Sundberg on 2013-02-15.
//  Copyright (c) 2013 Mikael Sundberg. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol msGeopardTag <NSObject>
-(NSDictionary *)GetAttributes;
-(NSString *)GetType;
-(void)ResignResponder;
-(void)TagSaved: (NSString *)withTagId;
@end
