//
//  msGeopardTagView.h
//  Geopard
//
//  Created by Mikael Sundberg on 2013-02-15.
//  Copyright (c) 2013 Mikael Sundberg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "msGeopardTag.h"

@interface msGeopardTagView : UIView <msGeopardTag>
{
    UIViewController *viewController;
    bool isNew;
}

-(void)SetController: (UIViewController *)controller;
-(UIViewController *)GetViewController;

@end
