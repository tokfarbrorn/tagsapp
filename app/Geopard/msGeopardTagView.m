//
//  msGeopardTagView.m
//  Geopard
//
//  Created by Mikael Sundberg on 2013-02-15.
//  Copyright (c) 2013 Mikael Sundberg. All rights reserved.
//

#import "msGeopardTagView.h"

@implementation msGeopardTagView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

-(NSDictionary *)GetAttributes
{
    NSDictionary *dict = [[NSDictionary alloc]init];
    return(dict);
}

-(NSString *)GetType
{
    return @"";
}

-(void)ResignResponder
{
}

-(void)SetController: (UIViewController *)controller;
{
    viewController  = controller;
}

-(UIViewController *)GetViewController;
{
    return viewController;
}

-(void)TagSaved: (NSString *)withTagId
{
    isNew = true;
}

@end
