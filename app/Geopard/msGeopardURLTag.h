//
//  msGeopardURLTag.h
//  Geopard
//
//  Created by Mikael Sundberg on 2013-02-15.
//  Copyright (c) 2013 Mikael Sundberg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "msGeopardTag.h"
#import "msGeopardTagView.h"

@interface msGeopardURLTag : msGeopardTagView <UITextFieldDelegate>
{
    bool urlCleared;
    bool tagsCleared;
}
@property (weak, nonatomic) IBOutlet UITextField *URL;
@property (weak, nonatomic) IBOutlet UITextField *Tags;

@end
