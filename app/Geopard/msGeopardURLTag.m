//
//  msGeopardURLTag.m
//  Geopard
//
//  Created by Mikael Sundberg on 2013-02-15.
//  Copyright (c) 2013 Mikael Sundberg. All rights reserved.
//

#import "msGeopardURLTag.h"

@implementation msGeopardURLTag

@synthesize URL;
@synthesize Tags;

- (id)initWithFrame:(CGRect)frame
{
    isNew = true;
    self = [super initWithFrame:frame];
    if (self)
    {
        [URL setDelegate:self];
        [Tags setDelegate:self];
    }
    return self;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if(!urlCleared && textField == URL)
    {
        URL.text = @"http://";
        urlCleared = true;
    }
    if(!tagsCleared && textField == Tags)
    {
        Tags.text = @"";
        tagsCleared = true;
    }
}

-(NSDictionary *)GetAttributes
{
    NSDictionary *dict = [[NSDictionary alloc]initWithObjectsAndKeys: self.Tags.text, @"tags",
                          self.URL.text, @"URL",
                          nil];
    return  dict;
}

-(NSString *)GetType
{
    return(@"url");
}

-(void)ResignResponder
{
    [Tags resignFirstResponder];
    [URL resignFirstResponder];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(void)TagSaved:(NSString *)withTagId
{
    [super TagSaved:withTagId];
    URL.text = @"http://";
    Tags.text = @"#tags";
    urlCleared = false;
    tagsCleared = false;
}

@end
