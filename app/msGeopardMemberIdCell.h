//
//  msGeopardMemberIdCell.h
//  Geopard
//
//  Created by Mikael Sundberg on 2013-02-21.
//  Copyright (c) 2013 Mikael Sundberg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface msGeopardMemberIdCell : UITableViewCell <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *MemberIdLabel;

@end
