//
//  msGeopardMemberIdCell.m
//  Geopard
//
//  Created by Mikael Sundberg on 2013-02-21.
//  Copyright (c) 2013 Mikael Sundberg. All rights reserved.
//

#import "msGeopardMemberIdCell.h"
#import "msGeopardAppDelegate.h"

@implementation msGeopardMemberIdCell

@synthesize MemberIdLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [MemberIdLabel resignFirstResponder];
    msGeopardAppDelegate *appDelegate = (msGeopardAppDelegate*)[[UIApplication sharedApplication]delegate];
    [appDelegate SetMemberId: [MemberIdLabel.text intValue]];
    [appDelegate SetNeedsReload];
    return YES;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [MemberIdLabel becomeFirstResponder];
}

@end
