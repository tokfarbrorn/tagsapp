//
//  msGeopardRangeCell.h
//  Geopard
//
//  Created by Mikael Sundberg on 2013-02-21.
//  Copyright (c) 2013 Mikael Sundberg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface msGeopardRangeCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UISlider *RangeSlider;
@property (weak, nonatomic) IBOutlet UILabel *RangeLabel;

@end
