//
//  msGeopardRangeCell.m
//  Geopard
//
//  Created by Mikael Sundberg on 2013-02-21.
//  Copyright (c) 2013 Mikael Sundberg. All rights reserved.
//

#import "msGeopardRangeCell.h"
#import "msGeopardAppDelegate.h"

@implementation msGeopardRangeCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)RangeValueChanged:(id)sender
{
    msGeopardAppDelegate *appDelegate = (msGeopardAppDelegate*)[[UIApplication sharedApplication]delegate];
    float d = self.RangeSlider.value;
    [appDelegate SetMaxRange:d];
    [appDelegate SetNeedsReload];
    self.RangeLabel.text = [[NSString alloc]initWithFormat:@"%0.0f m", d];
}

@end
