//
//  msGeopardSettingsController.h
//  Geopard
//
//  Created by Mikael Sundberg on 2013-02-21.
//  Copyright (c) 2013 Mikael Sundberg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "msGeopardRangeCell.h"
#import "msGeopardMemberIdCell.h"

@interface msGeopardSettingsController : UIViewController <UITableViewDataSource, UITableViewDelegate>
{
    msGeopardRangeCell *rangeCell;
    msGeopardMemberIdCell *memberCell;
}

@end
