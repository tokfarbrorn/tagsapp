//
//  msGeopardSettingsController.m
//  Geopard
//
//  Created by Mikael Sundberg on 2013-02-21.
//  Copyright (c) 2013 Mikael Sundberg. All rights reserved.
//

#import "msGeopardSettingsController.h"
#import "msGeopardAppDelegate.h"
#import "msGeopardRangeCell.h"
#import "msGeopardMemberIdCell.h"

@interface msGeopardSettingsController ()

@end

@implementation msGeopardSettingsController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(int)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if(section == 0)
        return @"Max distance to tags";
    else
        return @"Member ID";
}

-(int)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (IBAction)DoneButtonClick:(id)sender
{
    msGeopardAppDelegate *appDelegate = (msGeopardAppDelegate*)[[UIApplication sharedApplication]delegate];
    [appDelegate SetMemberId: [memberCell.MemberIdLabel.text intValue]];
    [appDelegate SetNeedsReload];

    UITabBarController *p = ((UITabBarController *)[self parentViewController]);
    [p setSelectedIndex:0];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{       
    
    msGeopardAppDelegate *appDelegate = (msGeopardAppDelegate*)[[UIApplication sharedApplication]delegate];

    if(indexPath.section == 0)
    {
        msGeopardRangeCell *cell = [tableView dequeueReusableCellWithIdentifier:@"rangeCell"];
        float distance = [appDelegate GetMaxRange];
        cell.RangeLabel.text = [[NSString alloc]initWithFormat:@"%0.0fm", distance];
        cell.RangeSlider.value = distance;
        rangeCell = cell;
        return cell;
    }
    else if(indexPath.section == 1)
    {
        msGeopardMemberIdCell *cell = [tableView dequeueReusableCellWithIdentifier:@"memberIdCell"];
        int memberid = [appDelegate GetMemberId];
        cell.MemberIdLabel.text = [[NSString alloc]initWithFormat:@"%i", memberid];
        memberCell = cell;
        return cell;
    }
    
    return(NULL);
    
}

@end
